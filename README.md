# libgnunet-messenger

A library for instant messaging based on the CADET subsystem of GNUnet.

## Notes

This repository is going to be deleted soon when I the moved source code gets out of the experimental state in the official repository of GNUnet.

Because the source code of the service and its CLI has been moved to the official repository of GNUnet the remaining parts got already removed. So this repository is just remaining for issues and others.
